main: build/libfinderInFile.so build/main.o
	@gcc -o build/mainLab2 -I src/ src/main.o -lfinderInFile -L/home/pablo/Escritorio/lab2sistope/build/ -lpthread -Wall -Wl,-rpath=/home/pablo/Escritorio/lab2sistope/build/
	@rm src/*.o
	@echo "Compilacion realizada"

build/libfinderInFile.so: src/findInFile.c
	@gcc -c -fPIC src/findInFile.c -o src/findInFile.o
	@gcc -shared -o build/libfinderInFile.so src/findInFile.o

build/main.o: src/main.o
	@gcc -c src/main.c -o src/main.o