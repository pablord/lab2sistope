#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include "findInFile.h"



int main(int argc, char*argv[]){
	unsigned int numHebras;
	unsigned int chunk;
	char *archivo;
	char *salida = NULL;
	char **queries;
	//int i;
	char* palabras;
	int sigOp;
	const char* const op_cortas="c:p:f:o:w:";
	const struct option op_largas[] = 
	  	{
	    	{ "chunk",1,NULL,'c'},
	    	{ "thread",1,NULL,'p'},
	    	{ "file",1,NULL,'f'},
	    	{ "output",1,NULL,'o'},
	    	{ "words",1,NULL,'w'},
	    	{ NULL,0,NULL,0}
	  	};
	while((sigOp=getopt_long(argc,argv,op_cortas,op_largas,NULL))!=-1){ 
		switch(sigOp){
			case 'c':	
				chunk = atoi(optarg);
				break;
			case 'p':	
				numHebras = atoi(optarg);
				break;
			case 'f':	
				archivo = optarg;
				break;
			case 'o':	
				salida = optarg;
				break;
			case 'w':	
				palabras = optarg;
				break;
			default:
				exit(EXIT_FAILURE); 
			}
	}
	queries=findInFile(archivo,palabras,chunk,numHebras);
	FILE*output;
	int i=0;
	if(queries==NULL){//si solo tiene elemento nulo, no se encontro nada
		printf("No se encontraron ocurrencias en el archivo\n");
	}else{//encontro al menos 1 ocurrencia y la imprime
		if(salida!=NULL){
			output=fopen(salida,"w");
			while(queries[i]!=NULL){
				strcat(queries[i],"\n");
				fputs(queries[i],output);
				
				i++;
			}
		}else{
			output=fopen("output.txt","w");
			while(queries[i]!=NULL){
				strcat(queries[i],"\n");
				fputs(queries[i],output);
				
				i++;
			}
		}
	}
		return 0;
}
