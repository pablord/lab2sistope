#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

typedef struct lista
{
	unsigned int inicio;
	unsigned int fin;
	struct lista *sig;
}bloques;
pthread_mutex_t m1;
pthread_mutex_t mwrite;
bloques *listaBloques=NULL;
char **find=NULL; //para retornar los resultados
char *file=NULL; //archivo que todos tienen que leer
char **stringsToSearch; // acá se guardan los strings que se pretenden buscar, todos deben tener acceso a esto
unsigned int tamanoArch; //guarda lo del ftell 
int numPalabras; // cantidad de palabras a buscar
int contEncontrada=0; // contador para hacer realloc en el char** find

bloques* quitarBloque(bloques*p){
	//elimina por delante
	bloques *aux;
	aux=p;
	if(p==NULL){
		return NULL;
	}else{
		aux=p->sig;
		free(p);
		return aux;
	}
}

bloques* fusionarBloque(bloques *p, int inicio){
	// fusiona dos bloques, si el fin del bloque que estoy acomodando actualmente śobrepasa al fin del siguiente, 
	// el siguiente debe ser borrado
	bloques *aux = p;
	while(aux!=NULL && aux->inicio!= inicio){
		aux=aux->sig;
	}
	bloques *borrar = aux->sig;
	aux->sig = borrar->sig;
	free(borrar);
	return p;
}


int* acomodarBloque(){
	int *iniciofin=(int *)malloc(2*sizeof(int)); // acá guardo el inicio y el fin para retornarlo
	pthread_mutex_lock(&m1); // SECCION CRITICA ESCRITURA EN LA LISTA, se puede correr más abajo creo, hay que revisar
	bloques *aux = listaBloques;
	if(aux==NULL){ // si no quedan bloques retorno -1
		iniciofin[0]=-1;
		iniciofin[1]=-1;
	}
	else{ 
		char * buffer = malloc(sizeof(char)*30000); // un char auxiliar
		iniciofin[0]=aux->inicio; // el inicio queda igual
		FILE *lectura = fopen(file,"r"); // se abre el archivo
		int sizeToRead = listaBloques->fin-listaBloques->inicio +1; // bytes a leer
		fseek(lectura,aux->inicio,SEEK_SET); // se avanza hasta la posición de inicio para leer
		fread(buffer,sizeof(unsigned int),sizeToRead,lectura); // se leen todos los bytes
		if(buffer[sizeToRead-1]=='\n' || buffer[sizeToRead-1]==EOF){ // si el ultimo caracter del bloque es \n
			iniciofin[1]=aux->fin; // queda todo igual
		}
		else if (fgetc(lectura)=='\n'){ // si el caracter que viene es \n sumo 1 al fin del bloque actual y 2 al inicio del siguiente
			iniciofin[1]= aux->fin +1;
			listaBloques->sig->inicio = iniciofin[1] +1;
		}else if(aux->fin==tamanoArch){
			iniciofin[1]=aux->fin;

		}
		else{ // en caso de que hayan más caracteres hacia la derecha
			fseek(lectura,aux->fin,SEEK_SET); // el fgetc corre el puntero del fichero, así que hay q dejarlo donde corresponde
			if(fgets(buffer, 1024,lectura)!=NULL){ // se lee todo hasta el proximo \n
				strncat(buffer,"\0",1); // se concatena \0 para que funcione strlen
				int offset = strlen(buffer); // se guarda el offset +1 por el \n que se comió el fgets (nose lo come)
				iniciofin[1] = aux->fin + offset; // se mueve el fin del bloque 
				aux->fin=iniciofin[1];
				bloques *borrar = aux; // puntero auxiliar	
				while((borrar->fin)>= (borrar->sig->fin)){ // si el fin del siguiente bloque es menor que el fin del actual
					listaBloques = fusionarBloque(listaBloques,borrar->sig->inicio); // se fusionan (se borra el bloque siguiente)
					 if (borrar->sig==NULL){
 						break;
 					}
				}
				if(listaBloques->sig!=NULL){
					listaBloques->sig->inicio = aux->fin+1;  // se mueve el inicio del siguiente
				}
			}
		}
		listaBloques = quitarBloque(listaBloques); // borro el bloque
		fclose(lectura);
	}
	pthread_mutex_unlock(&m1); // se acaba la sección crítica
	return iniciofin; // retorno inicio - fin del bloque
}
void *funcionThread(void *argumentos) {
	FILE *archivo = fopen(file,"r");
	int sizeRead;
	unsigned int *blockToRead;
	char *buffer;
	char *salto;
	char *espacio;
	int i;
	int contar;
	char *respaldo;
	char *save;
	int *marcar = (int *)calloc(numPalabras,sizeof(int)); // para marcar si se encontraron todas las palabras
	while(listaBloques!=NULL){
		blockToRead = acomodarBloque();
		if(blockToRead[0]!= blockToRead[1]){ // hay que  hacer esto por si EOF queda en un bloque, o cuando la funcion acomodar retorna -1
			sizeRead = blockToRead[1]-blockToRead[0]+1; // bytes a leer
			buffer = malloc((sizeRead+1)*sizeof(char)); // se asigna memoria al buffer
			fseek(archivo, blockToRead[0]-1,SEEK_SET); // se mueve el puntero del archivo
			fread(buffer,1,sizeRead,archivo); // se leen todos los bytes
			salto = strtok_r(buffer,"\n",&save); // se hace token por salto de linea
			strncat(salto,"\0",1);
			while(salto!=NULL){ // mientras queden lineas
				respaldo = malloc((strlen(salto)+1)*sizeof(char)); // se respalda la linea para guardarla en la lista
				strcpy(respaldo,salto); // se copia
				espacio = strtok(salto," "); // se hace token por espacio
				contar=0; // contador de coincidencias en 0
				marcar = calloc(numPalabras,sizeof(int)); // se inicializa en cero por cada linea el arreglo para marcar
				while(espacio!=NULL){ // mientras queden palabras 
					strncat(espacio,"\0",1); // se concatena \0
					for(i=0;i<numPalabras;i++){ // por cada palabra a buscar ingresadas por el usuario
						if(strcmp(espacio,stringsToSearch[i])==0 && marcar[i]==0){ // si se encuentra una palabra y no está marcada
							marcar[i] = 1; // se mmarca
							contar++; // y se incrementa el contador de coincidencias
						}
					}
					espacio = strtok(NULL," "); // se vuelve a hacer token
				}
				if(contar == numPalabras){ // si se encontraron todas las palabras
					pthread_mutex_lock(&mwrite); // SECCION CRITICA
				//	listaResultados = agregarCoincidencia(listaResultados, respaldo); // se escribe la coincidencia
					find=(char **)realloc(find,(contEncontrada+1)*sizeof(char *));
					find[contEncontrada++]=strdup(respaldo);
					pthread_mutex_unlock(&mwrite); // FIN SECCION CRITICA
				}
				salto = strtok_r(NULL,"\n",&save); // se hace token nuevamente por linea
				free(respaldo);
			}
			free(buffer);
		}
	}
	fclose(archivo); // se cierra el archivo
	pthread_exit(NULL); // fin thread
} 
bloques *agregarBloques(bloques *p,int inicio, int final){  // agrega bloque a la lista enlazada
	//agregar por delante
	bloques *nuevo;
	if(p==NULL){
		nuevo=(bloques*)malloc(sizeof(bloques));
		nuevo->inicio=inicio;
		nuevo->fin=final;
		nuevo->sig=NULL;
		return nuevo;
	}else{
		nuevo=(bloques*)malloc(sizeof(bloques));
		nuevo->inicio=inicio;
		nuevo->fin=final;
		nuevo->sig=p;
		return nuevo;
	}
}


char **findInFile(char *fileSearch, char *stringSearch, unsigned int sizeChunk, unsigned int numThreads){
	pthread_mutex_init(&m1, NULL);
	pthread_mutex_init(&mwrite, NULL);
	FILE *historial=fopen(fileSearch,"r");
	int final;
	int i;
	int inicioBloque=0;
	int finBloque;
	int cantBloques;
	if(historial==NULL){
		printf("El archivo indicado no existe o no se pudo abrir\n");
		exit(EXIT_FAILURE);
	}else{
		fseek(historial, 0L, SEEK_END);
        final = ftell(historial);	//numero de caracteres en total los enumera 1,2,3,4....
        tamanoArch=final;
        if(final%sizeChunk==0){
        	cantBloques=final/sizeChunk;
        }else{
        	cantBloques=(final/sizeChunk)+1;//bloque que no sea modulo
        }
        if(sizeChunk>final){ // si el archivo tiene más bytes que el chunk
        	inicioBloque=1;
        	finBloque=tamanoArch; // se crea sólo un bloque
        	listaBloques=agregarBloques(listaBloques,inicioBloque,finBloque);
        }else{       //crea la EDA de los bloques
	        for(i=0;i<cantBloques;i++){
	        	finBloque=final; //se crea el ultimo bloque primero como se agrega por delante
	        	inicioBloque=finBloque-(sizeChunk-1);
	        	if(inicioBloque<1){//en caso de no ser modulo los bloques con chunk
	        		inicioBloque=1;
	        	}
	        	listaBloques=agregarBloques(listaBloques,inicioBloque,finBloque);
				final=inicioBloque-1;        
	        }
    	}
        file = malloc(sizeof((fileSearch)+1)); // se guarda el nombre del archivo como variable global
        file=fileSearch;
       	char *auxiliar = strtok(stringSearch," "); 
       	int i = 0;
        while(auxiliar!=NULL){ // se guardan las palabras a buscar en un arreglo
        	strncat(auxiliar,"\0",1);
        	stringsToSearch = (char **)realloc(stringsToSearch,(i+1)*sizeof(char *));
        	stringsToSearch[i++]=strdup(auxiliar);
        	auxiliar = strtok(NULL," ");
        }
        numPalabras = i;
		pthread_t *arr_threads = (pthread_t *)malloc(numThreads*sizeof(pthread_t));
		//se crean n hebras
		for (i = 0; i<numThreads; i++) {
				pthread_create(&arr_threads[i], NULL, &funcionThread, NULL); 
		}
		//la hebra padre espera el resultado de las hijas para terminar la ejecucion
		for (i = 0; i <numThreads; i++){
			pthread_join(arr_threads[i],NULL);
		}
		// se deja el último elemento del char** NULL
		if(contEncontrada!=0){
			find[contEncontrada++]=NULL;
		}
	}
	return find;
}