Laboratorio 2 Sistemas Operativos.
Integrantes:
- Bryan Guzman Alvarez - 18.674.643-0
- Pablo Reyes Díaz - 18.058.223-1

Observaciones:
- En el archivo makefile deben ser reemplazada las rutas absolutas de la línea número 2 en los comandos -L y -rpath por la que contenga
a la carpeta build en los archivos extraidos del comprimido
-  El tamaño maximo del chunk debe ser menor al tamaño del buffer de lectura en el programa.

En el archivo se incluyen:
- La carpeta build, que se llena con el ejecutable y la biblioteca dinámica (.so) despues de la compilación.
- La carpeta src, que contiente los códigos del programa: main.c y findInFile.c y el header findInFile.h
- makefile para la compilación del programa
- Este archivo readme.

La finalidad de éste archivo es explicar paso a paso el proceso de compilación del programa.
Para compilar es necesario utilizar la terminal de Linux, y seguir los siguientes pasos:
- Abrir el directorio donde se ha descomprimido el archivo, utilizando el comando "cd", por ejemplo ~$ cd Escritorio
- Verificar que los archivos descritos anteriormente se encuentren en dicho directorio
- Escribir el comando "make" en la terminal.

Una vez realizados los pasos anteriormente descritos, el programa se compilara exitosamente.


Pasos para su ejecución.

Para ejemplificar el uso del programa, buscaremos las palabras "notas pep" en el archivo "ejemplo.txt" en el directorio /home/sistope/.

En la terminal de linux, para ejecutar el programa no importa el orden en que se ingresen los parámetros del programa, los cuales
son los siguientes:

->  "-c 5" o "--chunk 5" con esto, se entrega el tamaño de los bloques (en bytes) que tendrá que leer cada thread.
->  "-t 40" o "--thread 40" esto le dice al programa que habrán 40 threads trabajando en la búsqueda dentro del archivo
->  "-f "/home/sistope/ejemplo.txt" " o "--file "/home/sistope/ejemplo.txt" " le entrega al programa la ruta del archivo al que se le 
	quiere efectuar la búsqueda, que en éste caso es /home/sistope/ejemplo.txt
-> "-w "notas pep" " o --word "notas pep" " entrega al programa las palabras que se desean buscar.
-> "-o "salida.txt"" o "--output "salida.txt"" entrega al programa el archivo de salida en dónde se entregarán los resultados de la búsqueda
	(éste parámetro es opcional, en caso de no ingresarse se guardarán los resultados en /build/output.txt).

Es importante destacar que la ruta al archivo de entrada, las palabras a buscar y la ruta al archivo de salida deben ser ingresadas con
doble comillas, para evitar problemas con los archivos llamados, por ejemplo, "notas pep.txt".

Una vez que se ejecute el programa y termine exitosamente, se generará el archivo de salida en la ruta que el usuario ingrese 
(o en el archivo por defecto) con las coincidencias encontradas entre el archivo de entrada y las palabras ingresadas.